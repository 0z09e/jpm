import java.util.Scanner;
import java.util.Random;
import java.sql.*;
import java.util.Base64;

public class PasswordManager {
    static final String DB_URL = "jdbc:mysql://127.0.0.1/test";
    static final String USER = "0z09e";
    static final String PASS = "P@ssW0rd";
    
    public static String encryptor(String input, String key) {
        byte[] inputBytes = input.getBytes();
        byte[] keyBytes = key.getBytes();
        byte[] resultBytes = new byte[inputBytes.length];

        for (int i = 0; i < inputBytes.length; i++) {
            resultBytes[i] = (byte) (inputBytes[i] ^ keyBytes[i % keyBytes.length]);
        }

        return Base64.getEncoder().encodeToString(resultBytes);
    }

    public static String decryptor(String input, String key) {
        byte[] inputBytes = Base64.getDecoder().decode(input);
        byte[] keyBytes = key.getBytes();
        byte[] resultBytes = new byte[inputBytes.length];

        for (int i = 0; i < inputBytes.length; i++) {
            resultBytes[i] = (byte) (inputBytes[i] ^ keyBytes[i % keyBytes.length]);
        }

        return new String(resultBytes);
    }


    private static boolean addpassword(String superuser) {
        String password = "";
        Scanner sc = new Scanner(System.in);
        System.out.print("\n[*] Please enter your account URL or domain name : ");
        String domain = sc.next();
        System.out.print("\n[*] Please enter your username: ");
        String username = sc.next();
        System.out.println("\n[*] Do you want to us to generate a Super-Secure Password for you?");
        System.out.println("[*] 1 - Yes\n[*] 2 - No");
        while (!sc.hasNextInt()) {
                System.out.println("\n[-] Invalid input.");
                sc.next();
            }
        int password_confirmation = sc.nextInt();
        while (!(password_confirmation >= 1 && password_confirmation <= 2));
        switch (password_confirmation) {
            case 1:
                password = generatepassword();
                break;
            case 2:
                System.out.print("\n[*] Please enter your password : ");
                password = sc.next();
                break;
            }
        String encrypted_password = encryptor(password , superuser);
        try {
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();
            String sql = "INSERT INTO credentials (user_id, username, password, domain) VALUES ('" + superuser + "', '" + username + "', '" + encrypted_password + "', '" + domain + "');";
            stmt.executeUpdate(sql);
            mainMenu(superuser);
            return true;
        } catch (SQLException e) {
            System.out.println( e.getMessage());
            mainMenu(superuser);
            return false;
        }
    }

    private static String generatepassword() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("\n[*] Please enter the desired password length : ");
        int length = scanner.nextInt();
        String uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowercase = "abcdefghijklmnopqrstuvwxyz";
        String digits = "0123456789";
        String special = "!@#$%^&*()_+-=[]{}|;':\"<>,.?/";

        String allChars = uppercase + lowercase + digits + special;
        Random rand = new Random();
        StringBuilder password = new StringBuilder();

        for (int i = 0; i < length; i++) {
            int index = rand.nextInt(allChars.length());
            char randomChar = allChars.charAt(index);
            password.append(randomChar);
        }
        System.out.println("[+] Your password is : " + password.toString());
        return password.toString();
    }

    private static void readpassword(String superuser){

    try {
            Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
            Scanner scanner = new Scanner(System.in);
            System.out.print("\n[*] Enter the domain name of your stored password : ");
            String domain = scanner.nextLine();
            String query = "SELECT username, password FROM credentials WHERE domain = ? AND user_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, domain);
            preparedStatement.setString(2, superuser);
            ResultSet resultSet = preparedStatement.executeQuery();
            System.out.println("\n[+] Here's your Credentials");
            boolean found = false;
            while (resultSet.next()) {
                found = true;
                System.out.print("\n[+] Domain: " + domain);
                System.out.print(" | Username: " + resultSet.getString("username"));
                System.out.print(" | Password: " + decryptor(resultSet.getString("password") , superuser));
            } 
            if (!found) {
                System.out.println("\n[-] Error: Domain name not found in database.");
            }

            System.out.print("\n\n");
            mainMenu(superuser);
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            
            System.out.println("\n[-] Error: SQL exception occurred. " + e.getMessage());
            mainMenu(superuser);
        }
}


    private static boolean signup() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("\n[*] Enter your email: ");
        String email = scanner.nextLine();

        System.out.print("\n[*] Enter your username: ");
        String username = scanner.nextLine();

        System.out.print("\n[*] Enter your password: ");
        String password = scanner.nextLine();

        System.out.print("\n[*] Confirm your password: ");
        String confirmPassword = scanner.nextLine();

        if (!password.equals(confirmPassword)) {
            System.out.println("\n[-] Passwords do not match, please try again.");
            return false;
        }

        try {
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();

            String sql = "INSERT INTO users (email, username, password) VALUES ('" + email + "', '" + username + "', '" + password + "')";
            stmt.executeUpdate(sql);

            System.out.println("\n[+] Signup successful!");
            return true;
        } catch (SQLException e) {
            System.out.println("\n[-] Signup failed: " + e.getMessage());
            return false;
        }
    }

private static String signin() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("\n[*] Enter your username: ");
        String username = scanner.nextLine();

        System.out.print("\n[*] Enter your password: ");
        String password = scanner.nextLine();

        try {
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();

            String sql = "SELECT * FROM users WHERE username='" + username + "' AND password='" + password + "'";
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                System.out.println("\n[+] Signin successful!");
                return username;
            } else {
                System.out.println("\n[-] Signin failed, please check your username and password.");
                System.exit(0);
            }
        } catch (SQLException e) {
            System.out.println("\n[-] Signin failed: " + e.getMessage());
            System.exit(0);
        }
        return username;
    }

    private static boolean startup() {
        Scanner sc = new Scanner(System.in);

        System.out.println("\n_____________________________________________________\n    _         __        __      _    _     __  ______\n    /       /    )    /    )    /  ,'      /     /   \n---/-------/----/----/---------/_.'-------/-----/----\n  /       /    /    /         /  \\       /     /     \n_/____/__(____/____(____/____/____\\___ _/_ ___/______\n                                                     \n                                                     \n\n");
        while (true) {
        System.out.println("\n**** Lock-IT Password Manager ****");
        System.out.println("\n[*] 1 - Sign-in to Your Account\n[*] 2 - Create an Account\n[*] 3 - Exit");
        System.out.print("\n[*] Please select an option : ");
        int user_cnf = sc.nextInt();
        switch (user_cnf) {
            case 1:
                {
                    mainMenu(signin());
                }
            case 2:
                signup();
                continue;
            case 3:
                System.out.println("\n[*] Goodbye!");
                System.exit(0);
            default:
                System.out.println("\n[-] Invalid choice, please try again.");
                break;
        }
    }
}

    private static void mainMenu(String username){
        Scanner sc = new Scanner(System.in);
        System.out.println("\n****  DASHBOARD ****\n");
        System.out.println("\n[*] 1 - Add Password");
        System.out.println("[*] 2 - List Passwords");
        System.out.println("[*] 3 - Modify Passwords");
        System.out.println("[*] 4 - Delete Passwords");
        System.out.println("[*] 5 - Exit");
        int option = 0;

                do {
            System.out.print("\n[*] Please enter an option : ");
            while (!sc.hasNextInt()) {
                System.out.println("\n[-] Invalid input.");
                sc.next();
            }
            option = sc.nextInt();
        } while (!(option >= 1 && option <= 6));

                switch (option) {
            case 1:
                addpassword(username);
                break;
            case 2:
                readpassword(username);
                break;
            case 6:
                System.exit(0);
            }
    }


    public static void main(String[] args) {
        startup();

    }
}
