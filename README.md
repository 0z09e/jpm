# JPM - Java Password Manager

JPM is a Java-based password manager that uses XOR encryption to encrypt passwords and stores them in a MySQL database.

## Features

-   Create and store passwords securely in a MySQL database
-   Encrypt passwords using XOR encryption
-   Generate strong passwords
-   View and manage passwords
-   Backup and restore passwords

## Installation

1.  Install Java 8 or higher on your system.
2.  Install MySQL on your system and create a database for JPM.
3. Setup MySQL by creating 3 tables, Users and credential
4. Now add your creds to Password-Manager.java file
5. Compile the java file and run it.

## Encryption

JPM uses XOR encryption to encrypt passwords before storing them in the database. XOR encryption is a simple and fast encryption algorithm that uses a key to scramble the data.

JPM generates a unique key for each user based on their master password. The key is used to encrypt and decrypt passwords. This means that passwords are only accessible to the user who knows the master password.

## Database

JPM uses a MySQL database to store passwords. The database schema is created automatically when the application starts up.

## Contributing

If you find a bug or have a feature request, please create an issue on the JPM repository. Pull requests are welcome.

## License

JPM is released under the [MIT License](https://github.com/username/repo/blob/master/LICENSE).